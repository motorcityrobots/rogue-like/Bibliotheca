# Bibliotheca
## A collection of lists of words, phrases and names to be used in random generators.

### Index
1. warcry.csv - Example NPC battle cries that can be used to train a Markov chain generator.
2. names.csv - A list of names. Might want to split this into different name types such as humanNames.csv, monsterNames.csv, etc. For now just infodump into here.
3. itemAttributes.json - This file doesn't exist yet. I am keeping this here to help me remember to think about this. It will be a json with items and possible attributes for those items. Or maybe the other way around. Like I said I need to think about this one.